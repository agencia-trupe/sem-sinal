<?php

$lang['ftp_no_connection'] = 'N�o foi poss�vel localizar um ID de conex�o v�lido. Por favor, verifique que voc� est� conectado antes de executar qualquer rotina de arquivos.';
$lang['ftp_unable_to_connect'] = 'N�o foi poss�vel se conectar ao seu servidor de FTP utilizando o hostname fornecido.';
$lang['ftp_unable_to_login'] = 'N�o foi poss�vel se logar ao seu servidor de FTP. Por favor, verifique o nome de usu�rio e senha.';
$lang['ftp_unable_to_makdir'] = 'N�o foi poss�vel criar o diret�rio especificado.';
$lang['ftp_unable_to_changedir'] = 'N�o foi poss�vel alterar os diret�rios.';
$lang['ftp_unable_to_chmod'] = 'N�o foi poss�vel definir as permiss�es do arquivo. Por favor, verifique seu caminho. Nota: Esta fun��o s� est� dispon�vel no PHP 5 ou superior.';
$lang['ftp_unable_to_upload'] = 'N�o foi poss�vel enviar o arquivo especificado. Por favor, verifique seu caminho.';
$lang['ftp_unable_to_download'] = 'N�o foi poss�vel baixar o arquivo especificado. Por favor, verifique seu caminho.';
$lang['ftp_no_source_file'] = 'N�o foi poss�vel localizar o arquivo de origem. Por favor, verifique seu caminho.';
$lang['ftp_unable_to_rename'] = 'N�o foi poss�vel renomear o arquivo.';
$lang['ftp_unable_to_delete'] = 'N�o foi poss�vel deletar o arquivo.';
$lang['ftp_unable_to_move'] = 'N�o foi poss�vel mover o arquivo. Por favor, verifique se o diret�rio de destino existe.';
?>
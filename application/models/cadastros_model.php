<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastros_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = "cadastros";

        $this->dados = array();
        $this->dados_tratados = array();
	}

	function pegarTodos($atual, $preferencia){

		if($atual != 'todos')
			$this->db->where('operadora_atual', $atual);

		if($preferencia != 'todos')
			$this->db->where('operadora_preferencia', $preferencia);

		return $this->db->get($this->tabela)->result();
	}
}

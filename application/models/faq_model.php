<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'faq';
		
		$this->dados = array(
			'titulo',
			'texto'
		);
		$this->dados_tratados = array();
	}

	function pegarTodos(){
		return $this->db->order_by('ordem', 'asc')->get($this->tabela)->result();
	}
}
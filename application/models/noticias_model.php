<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'noticias';
		
		$this->dados = array(
			'titulo',
			'olho',
			'destino'
		);
		$this->dados_tratados = array();
	}

	function pegarTodos(){
		return $this->db->order_by('id', 'desc')->get($this->tabela)->result();
	}
}
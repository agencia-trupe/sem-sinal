<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends MY_Admincontroller {

    var $unidade,
        $titulo;

    function __construct(){
   		parent::__construct();

        $this->unidade = 'Questão';
        $this->titulo = 'Textos da Seção FAQ';

   		$this->load->model('faq_model', 'model');
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastros extends MY_Admincontroller {

    var $unidade,
        $titulo;

    function __construct(){
         parent::__construct();

         $this->unidade = 'Cadastro';
         $this->titulo = 'Cadastros';

         $this->load->model('cadastros_model', 'model');
    }

    function index($atual = 'todos', $preferencia = 'todos'){
        $data['registros'] = $this->model->pegarTodos($atual, $preferencia);

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $data['campo_1'] = $this->campo_1;
        $data['campo_2'] = $this->campo_2;
        $data['campo_3'] = $this->campo_3;
        $data['filtro_atual'] = $atual;
        $data['filtro_preferencia'] = $preferencia;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

   function extrair($atual = 'todos', $preferencia = 'todos'){
      $this->load->dbutil();

      $qrymanual = <<<QRY
SELECT cadastros.nome as 'nome',
cadastros.cpf as 'CPF',
cidades.nome as 'Cidade',
estados.uf as 'Estado',
cadastros.cep as 'CEP',
cadastros.endereco as 'Endereço',
cadastros.endereco_nr as 'Número',
cadastros.endereco_comp as 'Complemento',
cadastros.telefone_fixo as 'Telefone Fixo',
DATE_FORMAT(cadastros.data_nascimento, "%d/%m/%Y") as 'Data de Nascimento',
cadastros.email as 'E-mail',
cadastros.operadora_atual as 'Operadora Atual',
cadastros.operadora_preferencia as 'Operadora de Preferência',
cadastros.ddd_celular as 'DDD Celular',
cadastros.nr_celular as 'Celular',
CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(cadastros.media_gasto, 2), ".", "@"), ",", "."), "@", ",")) as 'Média de Gasto Mensal',
IF(cadastros.tipo_plano = 'pos_pago', 'Pós Pago', 'Pré Pago') as 'Tipo de Plano',
IF(cadastros.portabilidade = 1, 'Sim', 'Não') as 'Deseja Fazer Portabilidade?',
group_concat(DISTINCT lista_desejos.titulo separator '\n') as 'Desejos',
group_concat(DISTINCT lista_insatisfacoes.titulo separator '\n ') as 'Insatisfações'
FROM cadastros
LEFT JOIN tb_cidades cidades ON cadastros.id_cidade = cidades.id
LEFT JOIN tb_estados estados ON cadastros.id_uf = estados.id
LEFT JOIN rel_cadastros_desejos ON cadastros.id = rel_cadastros_desejos.id_cadastros
LEFT JOIN lista_desejos ON rel_cadastros_desejos.id_lista_desejos = lista_desejos.id
LEFT JOIN rel_cadastros_insatisfacoes ON cadastros.id = rel_cadastros_insatisfacoes.id_cadastros
LEFT JOIN lista_insatisfacoes ON rel_cadastros_insatisfacoes.id_lista_insatisfacoes = lista_insatisfacoes.id
QRY;

      $where = true;
      if ($atual != 'todos') {
         $qrymanual .= " WHERE cadastros.operadora_atual = '".$atual."'";
         $where = false;
      }
      if ($preferencia != 'todos') {
         if($where)
            $qrymanual .= " WHERE cadastros.operadora_preferencia = '".$preferencia."'";
         else
            $qrymanual .= " AND cadastros.operadora_preferencia = '".$preferencia."'";
      }

      $qrymanual .= " GROUP BY cpf;";

      $users = $this->db->order_by('nome', 'asc')->query($qrymanual);
      $filename = "cadastros_".Date('d-m-Y_H-i-s').'.csv';

      $delimiter = ",";
      $newline = "\r\n";

      $this->hasLayout = FALSE;
      $this->output->set_header('Content-Type: application/force-download');
      $this->output->set_header("Content-Disposition: attachment; filename='$filename'");
      $this->output->set_content_type('text/csv')->set_output($this->dbutil->csv_from_result($users, $delimiter, $newline));
   }

   function excluir($id){
	   if($this->model->excluir($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Usuário excluido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir usuário');
      }

      redirect('painel/cadastros/index/', 'refresh');
   }

}
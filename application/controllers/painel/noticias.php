<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends MY_Admincontroller {

    var $unidade,
        $titulo;

    function __construct(){
   		parent::__construct();

        $this->unidade = 'Notícia';
        $this->titulo = 'Notícias';

   		$this->load->model('noticias_model', 'model');
    }
}
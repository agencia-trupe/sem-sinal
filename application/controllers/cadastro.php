<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastro extends MY_Frontcontroller {

   var $operadoras;

   function __construct(){
   		parent::__construct();

         $this->operadoras = array(
            'claro',
            'oi',
            'tim',
            'vivo'
         );
   }

   function index($operadora = false){

   		$operadoras = $this->operadoras;

   		if(!$operadora || !in_array($operadora, $operadoras))
   			redirect('home/index');

   		$data['operadora_selecionada'] = $operadora;

         $data['estados'] = $this->db->order_by('uf', 'ASC')->get('tb_estados')->result();

         $data['lista_insatisfacoes'] = $this->db->order_by('id', 'ASC')->get('lista_insatisfacoes')->result();
         $data['lista_desejos'] = $this->db->order_by('id', 'ASC')->get('lista_desejos')->result();

         if(!$this->session->userdata('logged_in_cadastro'))
   		   $this->load->view('cadastro/novo', $data);
         else{

            $query_usuario = $this->db->get_where('cadastros', array('id' => $this->session->userdata('id')))->result();
            
            if(!isset($query_usuario[0])){
               redirect('login/logout');
            }

            $data['perfil'] = $query_usuario[0];

            $data['cidades'] = $this->db->get_where('tb_cidades', array('estado' => $data['perfil']->id_uf))->result();
            
            $qry_insatisfacoes = $this->db->select('id_lista_insatisfacoes')->get_where('rel_cadastros_insatisfacoes', array('id_cadastros' => $this->session->userdata('id')))->result_array();
            $qry_desejos = $this->db->select('id_lista_desejos')->get_where('rel_cadastros_desejos', array('id_cadastros' => $this->session->userdata('id')))->result_array();
            
            $data['rel_insatisfacoes'] = array();
            $data['rel_desejos'] = array();

            foreach ($qry_insatisfacoes as $key => $value) {
               array_push($data['rel_insatisfacoes'], $value['id_lista_insatisfacoes']);
            }

            foreach ($qry_desejos as $key => $value) {
               array_push($data['rel_desejos'], $value['id_lista_desejos']);
            }

            $this->load->view('cadastro/perfil', $data);
         }
   }

   function cadastrar(){

      $operadoras = $this->operadoras;

      $operadora_selecionada = $this->input->post('operadora_selecionada');
      $operadora_preferencia = $this->input->post('operadora_preferencia');
      $ddd_cel = $this->input->post("ddd_cel");
      $fone = $this->input->post("fone");
      $media_gasto = $this->input->post("media_gasto");
      $tipo_plano = $this->input->post("tipo_plano");
      $nome = $this->input->post("nome");
      $cpf = $this->input->post("cpf");
      $telefone_fixo = $this->input->post('telefone_fixo');
      $endereco = $this->input->post('endereco');
      $numero = $this->input->post('numero');
      $complemento = $this->input->post('complemento');
      $uf = $this->input->post("uf");
      $cidade = $this->input->post("cidade");
      $cep = $this->input->post("cep");
      $data_nascimento = $this->input->post("data_nascimento");
      $email = $this->input->post("email");
      $senha = $this->input->post("senha");
      $confirmacao_senha = $this->input->post("confirmacao_senha");
      $portar_numero = $this->input->post('portar_numero');
      $insatisfacoes = $this->input->post('insatisfacoes');
      $desejos = $this->input->post('desejo');

      $mensagem = "";

      if(!$operadora_selecionada)
         redirect('home');

      if(!$mensagem && !$operadora_preferencia)
         $mensagem = 'Informe a Operadora de sua Preferência!';

      if(!$mensagem && !in_array($operadora_selecionada, $operadoras))
         $mensagem = 'Informe uma operadora válida!';

      if(!$mensagem && !in_array($operadora_preferencia, $operadoras))
         $mensagem = 'Informe uma operadora válida!';
      
      if(!$mensagem && !$ddd_cel)
         $mensagem = 'Informe o DDD de seu telefone atual!';

      if(!$mensagem && !$fone)
         $mensagem = 'Informe o seu número de telefone atual!';
      
      if(!$mensagem && !$media_gasto)
         $mensagem = 'Informe seu o gasto médio mensal com seu celular!';

      if(!$mensagem && !$tipo_plano)
         $mensagem = 'Informe qual é o tipo do seu plano atual!';

      if(!$mensagem && !$nome)
         $mensagem = 'Informe seu Nome!';

      if(!$mensagem && strlen($nome) < 3)
         $mensagem = 'Informe seu Nome!';

      if(!$mensagem && !$cpf)
         $mensagem = 'Informe seu CPF!';

      if(!$mensagem && $this->db->get_where('cadastros', array('cpf' => $cpf))->num_rows() > 0 )
         $mensagem = 'CPF já cadastrado!';

      if(!$mensagem && !cpfValido($cpf))
         $mensagem = "Informe um cpf válido";

      if(!$mensagem && !$telefone_fixo)
         $mensagem = 'Informe seu Telefone Fixo!';

      if(!$mensagem && !$endereco)
         $mensagem = 'Informe seu Endereço!';

      if(!$mensagem && !$numero)
         $mensagem = 'Informe o Número de seu Endereço!';

      if(!$mensagem && !$uf)
         $mensagem = 'Informe seu estado!';

      if(!$mensagem && !$cidade)
         $mensagem = 'Informe sua cidade!';

      if(!$mensagem && !$cep)
         $mensagem = 'Informe seu CEP!';

      if(!$mensagem && !$data_nascimento)
         $mensagem = 'Informe sua data de nascimento!';

      if(!$mensagem && !dataValida($data_nascimento))
         $mensagem = 'Informe uma data de nascimento válida!';

      if(!$mensagem && !$email)
         $mensagem = 'Informe seu email!';

      if(!$mensagem && !emailValido($email))
         $mensagem = 'Informe um email válido!';

      if($this->db->get_where('cadastros', array('email' => $email))->num_rows() > 0)
         $mensagem = 'Email já cadastrado!';

      if(!$mensagem && !$senha)
         $mensagem = 'Informe sua senha de acesso!';

      if(!$mensagem && !$confirmacao_senha)
         $mensagem = 'Informe novamente sua senha de acesso!';

      if(!$mensagem && ($senha != $confirmacao_senha))
         $mensagem = 'A confirmação de senha não confere com a senha!';

      if($mensagem != ""){

         $this->session->set_flashdata('erro_validacao', $mensagem);
         redirect("cadastro/index/".$operadora_selecionada, 'refresh');

      }else{

         $insert = $this->db->set('nome', $nome)
                           ->set('cpf', $cpf)
                           ->set('id_uf', $uf)
                           ->set('id_cidade', $cidade)
                           ->set('cep', $cep)
                           ->set('endereco', $endereco)
                           ->set('endereco_nr', $numero)
                           ->set('endereco_comp', $complemento)
                           ->set('telefone_fixo', $telefone_fixo)
                           ->set('data_nascimento', formataData($data_nascimento, 'br2mysql'))
                           ->set('email', $email)
                           ->set('senha', criptografar($senha))
                           ->set('operadora_atual', $operadora_selecionada)
                           ->set('operadora_preferencia', $operadora_preferencia)
                           ->set('ddd_celular', $ddd_cel)
                           ->set('nr_celular', $fone)
                           ->set('media_gasto', money2float($media_gasto))
                           ->set('tipo_plano', $tipo_plano)
                           ->set('portabilidade', $portar_numero)
                           ->set('data_cadastro', Date('Y-m-d H:i:s'))
                           ->set('data_alteracao', Date('Y-m-d H:i:s'))
                           ->set('ip_cadastro', $_SERVER['REMOTE_ADDR'])
                           ->set('ip_alteracao', $_SERVER['REMOTE_ADDR'])
                           ->insert('cadastros');

         $id_usuario = $this->db->insert_id();          

         $this->db->where('id_cadastros', $id_usuario)->delete('rel_cadastros_insatisfacoes');
         if($insatisfacoes){
            foreach ($insatisfacoes as $key => $value) {
               $this->db->set('id_cadastros', $id_usuario)
                        ->set('id_lista_insatisfacoes', $value)
                        ->insert('rel_cadastros_insatisfacoes');
            }
         }

         $this->db->where('id_cadastros', $id_usuario)->delete('rel_cadastros_desejos');
         if($desejos){
            foreach ($desejos as $key => $value) {
               $this->db->set('id_cadastros', $id_usuario)
                        ->set('id_lista_desejos', $value)
                        ->insert('rel_cadastros_desejos');
            }
         }
   
         $this->session->set_flashdata('validacao_ok', TRUE);
         redirect("cadastro/index/".$operadora_selecionada, 'refresh');
      }
   }

   function atualizar(){

      $operadoras = $this->operadoras;

      $operadora_selecionada = $this->input->post('operadora_atual');
      $operadora_preferencia = $this->input->post('operadora_preferencia');
      $ddd_cel = $this->input->post("ddd_cel");
      $fone = $this->input->post("fone");
      $media_gasto = $this->input->post("media_gasto");
      $tipo_plano = $this->input->post("tipo_plano");
      $nome = $this->input->post("nome");      
      $telefone_fixo = $this->input->post('telefone_fixo');
      $endereco = $this->input->post('endereco');
      $numero = $this->input->post('numero');
      $complemento = $this->input->post('complemento');
      $uf = $this->input->post("uf");
      $cidade = $this->input->post("cidade");
      $cep = $this->input->post("cep");
      $data_nascimento = $this->input->post("data_nascimento");
      $email = $this->input->post("email");
      $senha = $this->input->post("senha");
      $confirmacao_senha = $this->input->post("confirmacao_senha");

      $portar_numero = $this->input->post('portar_numero');
      $insatisfacoes = $this->input->post('insatisfacoes');
      $desejos = $this->input->post('desejo');

      $mensagem = "";

      if(!$mensagem && !$operadora_preferencia)
         $mensagem = 'Informe a Operadora de sua Preferência!';

      if(!$mensagem && !in_array($operadora_selecionada, $operadoras))
         $mensagem = 'Informe uma operadora válida!';

      if(!$mensagem && !in_array($operadora_preferencia, $operadoras))
         $mensagem = 'Informe uma operadora válida!';
      
      if(!$mensagem && !$ddd_cel)
         $mensagem = 'Informe o DDD de seu telefone atual!';

      if(!$mensagem && !$fone)
         $mensagem = 'Informe o seu número de telefone atual!';
      
      if(!$mensagem && !$media_gasto)
         $mensagem = 'Informe seu o gasto médio mensal com seu celular!';

      if(!$mensagem && !$tipo_plano)
         $mensagem = 'Informe qual é o tipo do seu plano atual!';

      if(!$mensagem && !$nome)
         $mensagem = 'Informe seu Nome!';

      if(!$mensagem && strlen($nome) < 3)
         $mensagem = 'Informe seu Nome!';

      if(!$mensagem && !$uf)
         $mensagem = 'Informe seu estado!';

      if(!$mensagem && !$cidade)
         $mensagem = 'Informe sua cidade!';

      if(!$mensagem && !$cep)
         $mensagem = 'Informe seu CEP!';

      if(!$mensagem && !$data_nascimento)
         $mensagem = 'Informe sua data de nascimento!';

      if(!$mensagem && !dataValida($data_nascimento))
         $mensagem = 'Informe uma data de nascimento válida!';

      if(!$mensagem && !$telefone_fixo)
         $mensagem = 'Informe seu Telefone Fixo!';

      if(!$mensagem && !$endereco)
         $mensagem = 'Informe seu Endereço!';
      
      if(!$mensagem && !$numero)
         $mensagem = 'Informe o Número de seu Endereço!';

      if(!$mensagem && !$email)
         $mensagem = 'Informe seu email!';

      if(!$mensagem && !emailValido($email))
         $mensagem = 'Informe um email válido!';

      if($this->db->get_where('cadastros', array('email' => $email, 'id !=' => $this->session->userdata('id')))->num_rows() > 0)
         $mensagem = 'Email já cadastrado!';      

      if($senha){

         if(!$mensagem && !$confirmacao_senha)
            $mensagem = 'Informe novamente sua senha de acesso!';

         if(!$mensagem && ($senha != $confirmacao_senha))
            $mensagem = 'A confirmação de senha não confere com a senha!';

      }

      if($mensagem != ""){

         $this->session->set_flashdata('erro_validacao', $mensagem);
         redirect("cadastro/index/".$operadora_selecionada, 'refresh');

      }else{

         $update = $this->db->set('nome', $nome)
                           ->set('id_uf', $uf)
                           ->set('id_cidade', $cidade)
                           ->set('cep', $cep)
                           ->set('endereco', $endereco)
                           ->set('endereco_nr', $numero)
                           ->set('endereco_comp', $complemento)
                           ->set('telefone_fixo', $telefone_fixo)
                           ->set('data_nascimento', formataData($data_nascimento, 'br2mysql'))
                           ->set('email', $email)
                           ->set('operadora_atual', $operadora_selecionada)
                           ->set('operadora_preferencia', $operadora_preferencia)
                           ->set('ddd_celular', $ddd_cel)
                           ->set('nr_celular', $fone)
                           ->set('media_gasto', money2float($media_gasto))
                           ->set('tipo_plano', $tipo_plano)
                           ->set('portabilidade', $portar_numero)
                           ->set('data_alteracao', Date('Y-m-d H:i:s'))
                           ->set('ip_alteracao', $_SERVER['REMOTE_ADDR'])
                           ->where('id', $this->session->userdata('id'))
                           ->update('cadastros');
         $last = $this->db->last_query();

         if($senha){
            $this->db->set('senha', criptografar($senha))->where('id', $this->session->userdata('id'))->update('cadastros');
         }

         $id_usuario = $this->session->userdata('id');

         $this->db->where('id_cadastros', $id_usuario)->delete('rel_cadastros_insatisfacoes');
         if($insatisfacoes){
            foreach ($insatisfacoes as $key => $value) {
               $this->db->set('id_cadastros', $id_usuario)
                        ->set('id_lista_insatisfacoes', $value)
                        ->insert('rel_cadastros_insatisfacoes');
            }
         }

         $this->db->where('id_cadastros', $id_usuario)->delete('rel_cadastros_desejos');
         if($desejos){
            foreach ($desejos as $key => $value) {
               $this->db->set('id_cadastros', $id_usuario)
                        ->set('id_lista_desejos', $value)
                        ->insert('rel_cadastros_desejos');
            }
         }
   
         $this->session->set_flashdata('validacao_ok', TRUE);
         redirect("cadastro/index/".$operadora_selecionada, 'refresh');
      }
   }

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){

    	$data['questoes'] = $this->db->order_by('ordem', 'asc')->get('faq')->result();

   		$this->load->view('faq', $data);
    }

}

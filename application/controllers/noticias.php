<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){

    	$data['noticias'] = $this->db->order_by('id', 'desc')->get('noticias')->result();

   		$this->load->view('noticias', $data);
    }


}
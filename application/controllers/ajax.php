<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

   	function __construct(){
   		parent::__construct();

   		if(!$this->input->is_ajax_request())
   			redirect('home');
   	}

   	function pegaCidades(){

   		$id_estado = $this->input->post('estado');

   		echo json_encode($this->db->get_where('tb_cidades', array('estado' => $id_estado))->result());

   	}

}

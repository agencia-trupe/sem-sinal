<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

      $this->load->library('logincadastros');
    }

    function index(){
   		$login = $this->input->post('login');
   		$senha = $this->input->post('senha');

   		if(!$login || !$senha)
   			redirect('home/index');

   		if(!$this->logincadastros->login($login, $senha))
            $this->session->set_flashdata('errlogin', true);

        redirect($this->session->userdata('redirect'));
    }

    function logout(){
      $this->logincadastros->logout();
      redirect('home/index');
    }

    function recuperacao(){
      $this->load->view('recuperar');
    }

    function recuperar(){
      $cpf = $this->input->post('cpf');

      $mensagem = "";

      if(!$cpf)
        $mensagem = "Informe seu CPF!";
      else{
        
        if(!$mensagem && $this->db->get_where('cadastros', array('cpf' => $cpf))->num_rows() == 0)
          $mensagem = "CPF Não encontrado em nosso sistema!";

        if(!$mensagem){
          $query = $this->db->get_where('cadastros', array('cpf' => $cpf))->result();
          $cadastro = $query[0];
          $email = $query[0]->email;

          if(!$mensagem && !$email)
            $mensagem = "Nenhum email foi encontrado!";
        }

      }

      if($mensagem){
        $this->session->set_flashdata('erro_validacao', $mensagem);        
      }else{
        $this->session->set_flashdata('validacao_ok', $mensagem);
        $this->enviaSenha($cpf, $email);
      }

      redirect('login/recuperar', 'refresh');

    }

    function enviaSenha($cpf, $email){

      $emailconf['charset'] = 'utf-8';
      $emailconf['mailtype'] = 'html';
      $emailconf['protocol'] = 'smtp';

      $emailconf['smtp_host'] = 'smtp@semsinal.com.br';
      $emailconf['smtp_user'] = 'noreply';
      $emailconf['smtp_pass'] = 'noreply277';
      $emailconf['smtp_port'] = 587;

      $emailconf['crlf'] = "\r\n";
      $emailconf['newline'] = "\r\n";

      $this->load->library('email');
      $this->email->initialize($emailconf);

      $novaSenha = randomPassword();

      $this->db->set('senha', criptografar($novaSenha))->where('cpf', $cpf)->update('cadastros');
      $from = 'noreply@semsinal.com.br';
      $fromname = 'Recuperação de Senha';

      $msg1 = "Sua senha temporária para acesso ao sistema é";
      $msg2 = "Faça o login visitando nosso site";

      $to = $email;
      $bc = FALSE;
      //$bcc = FALSE;

      $bcc = 'bruno@trupe.net';

      $assunto = 'Recuperação de Senha';

      $email = "<html>
                  <head>
                      <style type='text/css'>
                          .tit{
                              font-weight:bold;
                              font-size:14px;
                              color:#5DB8C9;
                              font-family:Arial;
                          }
                          .val{
                              color:#000;
                              font-size:12px;
                              font-family:Arial;
                          }
                      </style>
                  </head>
                  <body>                   

                  $msg1 : $novaSenha<br>
                  $msg2 : <a href='http://www.semsinal.com.br'>semsinal.com.br</a>
                  </body>
                  </html>";

      $this->email->from($from, $fromname);
      $this->email->to($to);

      if($bc)
          $this->email->cc($bc);
      if($bcc)
          $this->email->bcc($bcc);

      $this->email->reply_to($email);
      $this->email->subject($assunto);
      $this->email->message($email);
      $this->email->send();
    }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
   		$this->load->view('contato');
    }

   function enviar(){
   		$emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';

        
        $emailconf['protocol'] = 'smtp';
        $emailconf['smtp_host'] = 'smtp@semsinal.com.br';
        $emailconf['smtp_user'] = 'noreply';
        $emailconf['smtp_pass'] = 'noreply277';
        $emailconf['smtp_port'] = 587;
        $emailconf['crlf'] = "\r\n";
        $emailconf['newline'] = "\r\n"; 
		
        /*
        contato
        contat277
        */
        $this->load->library('email');

        $this->email->initialize($emailconf);

        $u_nome = $this->input->post('nome');
        $u_telefone = $this->input->post('telefone');
        $u_cpf = $this->input->post('cpf');
        $u_email = $this->input->post('email');
        $u_msg = $this->input->post('mensagem');

        $mensagem = "";

        if(!$mensagem && !$u_nome)
            $mensagem = 'Informe seu Nome!';

        if(!$mensagem && !$u_email)
            $mensagem = 'Informe seu Email!';

        if(!$mensagem && !$u_cpf)
            $mensagem = 'Informe seu CPF!';

        if($mensagem){
            $this->session->set_flashdata('erro_validacao', $mensagem);
            redirect("contato/index/", 'refresh');            
        }

        $from = "noreply@semsinal.com.br";
        $fromname = 'Contato via Site';

        $to = "contato@semsinal.com.br";
        $bc = FALSE;
        $bcc = 'bruno@trupe.net';

        $assunto = 'Contato via Site';
        $email = "<html>
                    <head>
                        <style type='text/css'>
                            .tit{
                                font-weight:bold;
                                font-size:14px;
                                color:#5DB8C9;
                                font-family:Arial;
                            }
                            .val{
                                color:#000;
                                font-size:12px;
                                font-family:Arial;
                            }
                        </style>
                    </head>
                    <body>
                    <span class='tit'>Nome :</span> <span class='val'>$u_nome</span><br />
                    <span class='tit'>Telefone :</span> <span class='val'>$u_telefone</span><br />
                    <span class='tit'>CPF :</span> <span class='val'>$u_cpf</span><br />
                    <span class='tit'>Email :</span> <span class='val'>$u_email</span><br />
                    <span class='tit'>Mensagem :</span> <span class='val'>$u_msg</span>
                    </body>
                    </html>";

        $this->email->from($from, $fromname);
        $this->email->to($to);

        if($bc)
            $this->email->cc($bc);
        if($bcc)
            $this->email->bcc($bcc);

        $this->email->reply_to($u_email);

        $this->email->subject($assunto);
        $this->email->message($email);

	    //$this->email->send();

        $this->session->set_flashdata('envio', TRUE);
        redirect('contato/index' , 'refresh');
   }
}

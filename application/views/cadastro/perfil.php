<div id="form">

	<h1>Seu Plano e Sua Operadora</h1>

	<form method="post" id="form-cadastro" action="cadastro/atualizar">

		<h2>Sua Operadora <span style="color:#990000;">atual</span>:</h2>

		<div class="caixa-celulares">
			<label class="<?if($perfil->operadora_atual=='claro')echo " selecionado"; else echo " deselecionado"?>">
				<input type="radio" class="sel-operad" id="sel-operad-claro" name="operadora_atual" <?if($perfil->operadora_atual=='claro')echo "checked"?> value="claro">
				<img src="_imgs/layout/home-celular-claro.png" alt="claro">
			</label>
			<label class="<?if($perfil->operadora_atual=='tim')echo " selecionado"; else echo " deselecionado"?>">
				<input type="radio" class="sel-operad" id="sel-operada-tim" name="operadora_atual" <?if($perfil->operadora_atual=='tim')echo "checked"?> value="tim">
				<img src="_imgs/layout/home-celular-tim.png" alt="tim">
			</label>
			<label class="<?if($perfil->operadora_atual=='vivo')echo " selecionado"; else echo " deselecionado"?>">
				<input type="radio" class="sel-operad" id="sel-operada-vivo" name="operadora_atual" <?if($perfil->operadora_atual=='vivo')echo "checked"?> value="vivo">
				<img src="_imgs/layout/home-celular-vivo.png" alt="vivo">
			</label>
			<label class="<?if($perfil->operadora_atual=='oi')echo " selecionado"; else echo " deselecionado"?>">
				<input type="radio" class="sel-operad" id="sel-operada-oi" name="operadora_atual" <?if($perfil->operadora_atual=='oi')echo "checked"?> value="oi">
				<img src="_imgs/layout/home-celular-oi.png" alt="oi">
			</label>
		</div>

		<h2>Sua Operadora de <span style="color:#990000;">preferência</span>:</h2>

		<div class="caixa-celulares">
			<label class="<?if($perfil->operadora_preferencia=='claro')echo " selecionado"; else echo " deselecionado"?>">
				<input type="radio" class="sel-operad" id="sel-operad-claro" name="operadora_preferencia" <?if($perfil->operadora_preferencia=='claro')echo "checked"?> value="claro">
				<img src="_imgs/layout/home-celular-claro.png" alt="claro">
			</label>
			<label class="<?if($perfil->operadora_preferencia=='tim')echo " selecionado"; else echo " deselecionado"?>">
				<input type="radio" class="sel-operad" id="sel-operad-tim" name="operadora_preferencia" <?if($perfil->operadora_preferencia=='tim')echo "checked"?> value="tim">
				<img src="_imgs/layout/home-celular-tim.png" alt="tim">
			</label>
			<label class="<?if($perfil->operadora_preferencia=='vivo')echo " selecionado"; else echo " deselecionado"?>">
				<input type="radio" class="sel-operad" id="sel-operad-vivo" name="operadora_preferencia" <?if($perfil->operadora_preferencia=='vivo')echo "checked"?> value="vivo">
				<img src="_imgs/layout/home-celular-vivo.png" alt="vivo">
			</label>
			<label class="<?if($perfil->operadora_preferencia=='oi')echo " selecionado"; else echo " deselecionado"?>">
				<input type="radio" class="sel-operad" id="sel-operad-oi" name="operadora_preferencia" <?if($perfil->operadora_preferencia=='oi')echo "checked"?> value="oi">
				<img src="_imgs/layout/home-celular-oi.png" alt="oi">
			</label>
		</div>

		<h2>Seu número de Celular:</h2>

		<div class="fone-container">
			<input type="text" name="ddd_cel" maxlength="3" id="input-ddd" value="<?=$perfil->ddd_celular?>">
			<input type="text" name="fone" maxlength="10" id="input-fone" value="<?=$perfil->nr_celular?>">
		</div>

		<h2>Sua Média de Gasto Mensal:</h2>

		<div class="fone-container">
			<input type="text" name="media_gasto" id="media-gasto" value="<?=float2money($perfil->media_gasto)?>">
		</div>

		<h2>Seu tipo de plano:</h2>

		<div class="c-align botoes">
			<label class="com-margem <?if($perfil->tipo_plano=='pre_pago')echo " selecionado"?>"><input type="radio" name="tipo_plano" value="pre_pago" <?if($perfil->tipo_plano=='pre_pago')echo " checked"?>>PRÉ PAGO</label>
			<label<?if($perfil->tipo_plano=='pos_pago')echo " class='selecionado'"?>><input type="radio" name="tipo_plano" value="pos_pago" <?if($perfil->tipo_plano=='pos_pago')echo " checked"?>>PÓS PAGO</label>
		</div>

		<div class="main-form">

			<label class="large">
				Nome*<br>
				<input type="text" name="nome" value="<?=$perfil->nome?>">
			</label>

			<label class="short com-margem">
				CPF*<br>
				<input type="text" disabled id="input-cpf" value="<?=$perfil->cpf?>">
			</label>

			<label class="short">
				Telefone Fixo<br>
				<input type="text" name="telefone_fixo" id="input-fixo" value="<?=$perfil->telefone_fixo?>">
			</label>

			<label class="large">
				Endereço/Logradouro<br>
				<input type="text" name="endereco" value="<?=$perfil->endereco?>">
			</label>

			<label class="short com-margem">
				Número<br>
				<input type="text" name="numero" value="<?=$perfil->endereco_nr?>">
			</label>

			<label class="short">
				Complemento<br>
				<input type="text" name="complemento" value="<?=$perfil->endereco_comp?>">
			</label>

			<br>

			<label class="short uf com-margem">
				UF*<br>
				<select name="uf" id="sel-uf">
					<option value=""></option>
					<?php foreach ($estados as $key => $value): ?>
						<option value="<?=$value->id?>" <?if($value->id==$perfil->id_uf)echo " selected"?>><?=$value->uf?></option>	
					<?php endforeach ?>
				</select>
			</label>

			<label class="short cidade">
				Cidade*<br>
				<select name="cidade" id="sel-cidade">
					<?php foreach ($cidades as $key => $value): ?>
						<option value="<?=$value->id?>" <?if($value->id==$perfil->id_cidade)echo " selected"?>><?=$value->nome?></option>	
					<?php endforeach ?>					
				</select>
			</label>

			<br>

			<label class="short com-margem">
				CEP*<br>
				<input type="text" name="cep" id="input-cep" value="<?=$perfil->cep?>">
			</label>

			<label class="short">
				Data de Nascimento*<br>
				<input type="text" name="data_nascimento" id="input-data" value="<?=formataData($perfil->data_nascimento, 'mysql2br')?>">
			</label>

			<label class="large">
				E-mail*<br>
				<input type="email" id="input-email" name="email" value="<?=$perfil->email?>">
			</label>

			<label class="short com-margem">
				Senha*<br>
				<input type="password" name="senha" id="input-senha">
			</label>

			<label class="short">
				Confirmar Senha*<br>
				<input type="password" name="confirmacao_senha" id="input-confirmacao_senha">
			</label>

			<label class="large portar">
				<input type="checkbox" name="portar_numero" value="1" <?if($perfil->portabilidade)echo' checked'?>> Sim, quero portar meu número.
			</label>

			<div class="coluna menor">
				<h2>Insatisfeito Com...</h2>
				<ul>
					<?php foreach ($lista_insatisfacoes as $key => $value): ?>
						<li><label<?if(in_array($value->id, $rel_insatisfacoes))echo" class='selecionado'"?>><input type="checkbox" name="insatisfacoes[]" <?if(in_array($value->id, $rel_insatisfacoes))echo" checked"?> value="<?=$value->id?>"><?=$value->titulo?></label></li>	
					<?php endforeach ?>
				</ul>
			</div>

			<div class="coluna">
				<h2>Quero da Nova Operadora...</h2>
				<ul>
					<?php foreach ($lista_desejos as $key => $value): ?>
						<li><label<?if(in_array($value->id, $rel_desejos))echo" class='selecionado'"?>><input type="checkbox" name="desejo[]" <?if(in_array($value->id, $rel_desejos))echo" checked"?>  value="<?=$value->id?>"><?=$value->titulo?></label></li>
					<?php endforeach ?>
				</ul>
			</div>

			<input type="hidden" id="input-ed-form">

		</div>
		<input type="reset" id="btn-reset">
		<input type="submit" value="ENVIAR">

	</form>

</div>

<?php if ($this->session->flashdata('erro_validacao')): ?>
	<script defer>
		$('document').ready( function(){
			alerta("<?=$this->session->flashdata('erro_validacao')?>");
		});
	</script>
<?php endif; ?>

<?php if ($this->session->flashdata('validacao_ok')): ?>
	<script defer>
		$('document').ready( function(){
			var retorno = "<h2>Dados atualizados com sucesso!</h2>";
			retorno += "<p>Compartilhe essa novidade e ajude a mudar a qualidade dos serviços das operadoras!</p>";
	        retorno += "<div style='height:26px; margin:8px 0;'><div class='fb-like' data-href='http://www.semsinal.com.br' data-send='false' data-width='80' data-show-faces='false'></div>";
	        retorno += "<span class='vermelho'>Compartilhe!</span></div>";
			alerta(retorno);
		});
	</script>
<?php endif; ?>
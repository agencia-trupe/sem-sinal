<div id="form">

	<h1>Seu Plano e Sua Operadora</h1>

	<form method="post" id="form-cadastro" action="cadastro/cadastrar">

		<input type="hidden" name="operadora_selecionada" value="<?=$operadora_selecionada?>">

		<h2>Qual é a Operadora de sua preferência?</h2>

		<div class="caixa-celulares">
			<label>
				<input type="radio" class="sel-operad" id="sel-operad-claro" name="operadora_preferencia" value="claro">
				<img src="_imgs/layout/home-celular-claro.png" alt="claro">
			</label>
			<label>
				<input type="radio" class="sel-operad" id="sel-operad-tim" name="operadora_preferencia" value="tim">
				<img src="_imgs/layout/home-celular-tim.png" alt="tim">
			</label>
			<label>
				<input type="radio" class="sel-operad" id="sel-operad-vivo" name="operadora_preferencia" value="vivo">
				<img src="_imgs/layout/home-celular-vivo.png" alt="vivo">
			</label>
			<label>
				<input type="radio" class="sel-operad" id="sel-operad-oi" name="operadora_preferencia" value="oi">
				<img src="_imgs/layout/home-celular-oi.png" alt="oi">
			</label>
		</div>
		
		<h2>Qual é o seu número de Celular?</h2>

		<div class="fone-container">
			<input type="text" name="ddd_cel" maxlength="3" id="input-ddd">
			<input type="text" name="fone" maxlength="10" id="input-fone">
		</div>

		<h2>Média de Gasto Mensal</h2>

		<div class="fone-container">
			<input type="text" name="media_gasto" id="media-gasto">
		</div>

		<h2>Qual é o tipo do seu plano de telefonia celular?</h2>

		<div class="c-align botoes">
			<label class="com-margem"><input type="radio" name="tipo_plano" value="pre_pago">PRÉ PAGO</label>
			<label><input type="radio" name="tipo_plano" value="pos_pago">PÓS PAGO</label>
		</div>

		<div class="main-form">

			<label class="large">
				Nome*<br>
				<input type="text" name="nome">
			</label>

			<label class="short com-margem">
				CPF*<br>
				<input type="text" name="cpf" id="input-cpf">
			</label>

			<label class="short">
				Telefone Fixo*<br>
				<input type="text" name="telefone_fixo" id="input-fixo">
			</label>

			<label class="large">
				Endereço/Logradouro*<br>
				<input type="text" name="endereco">
			</label>

			<label class="short com-margem">
				Número*<br>
				<input type="text" name="numero">
			</label>

			<label class="short">
				Complemento<br>
				<input type="text" name="complemento">
			</label>

			<br>

			<label class="short uf com-margem">
				UF*<br>
				<select name="uf" id="sel-uf">
					<option value=""></option>
					<?php foreach ($estados as $key => $value): ?>
						<option value="<?=$value->id?>"><?=$value->uf?></option>	
					<?php endforeach ?>
				</select>
			</label>

			<label class="short cidade">
				Cidade*<br>
				<select name="cidade" id="sel-cidade">
					<option value=""></option>					
				</select>
			</label>

			<br>

			<label class="short com-margem">
				CEP*<br>
				<input type="text" name="cep" id="input-cep">
			</label>

			<label class="short">
				Data de Nascimento*<br>
				<input type="text" name="data_nascimento" id="input-data">
			</label>

			<label class="large">
				E-mail*<br>
				<input type="email" name="email" id="input-email">
			</label>

			<label class="short com-margem">
				Senha*<br>
				<input type="password" name="senha" id="input-senha">
			</label>

			<label class="short">
				Confirmar Senha*<br>
				<input type="password" name="confirmacao_senha" id="input-confirmacao_senha">
			</label>

			<label class="large portar">
				<input type="checkbox" name="portar_numero" value="1"> Sim, quero portar meu número.
			</label>

			<div class="coluna menor">
				<h2>Insatisfeito Com...</h2>
				<ul>
					<?php foreach ($lista_insatisfacoes as $key => $value): ?>
						<li><label><input type="checkbox" name="insatisfacoes[]" value="<?=$value->id?>"><?=$value->titulo?></label></li>	
					<?php endforeach ?>
				</ul>
			</div>

			<div class="coluna">
				<h2>Quero da Nova Operadora...</h2>
				<ul>
					<?php foreach ($lista_desejos as $key => $value): ?>
						<li><label><input type="checkbox" name="desejo[]" value="<?=$value->id?>"><?=$value->titulo?></label></li>
					<?php endforeach ?>
				</ul>
			</div>

		</div>
		<input type="reset" id="btn-reset">
		<input type="submit" value="ENVIAR">

	</form>

</div>

<?php if ($this->session->flashdata('erro_validacao')): ?>
	<script defer>
		$('document').ready( function(){
			alerta("<?=$this->session->flashdata('erro_validacao')?>");
		});
	</script>
<?php endif; ?>

<?php if ($this->session->flashdata('validacao_ok')): ?>
	<script defer>
		$('document').ready( function(){
			var retorno = "<h2>Obrigado pelo seu cadastro!</h2>";
	        retorno += "<p>Em breve você deverá receber um contato. </p>";
	        retorno += "<p>Compartilhe essa novidade e ajude a mudar a qualidade dos serviços das operadoras!</p>";
	        retorno += "<div style='height:26px; margin:8px 0;'><div class='fb-like' data-href='http://www.semsinal.com.br' data-send='false' data-width='80' data-show-faces='false'></div>";
	        retorno += "<span class='vermelho'>Compartilhe!</span></div>";
			alerta(retorno);
		});
	</script>
<?php endif; ?>
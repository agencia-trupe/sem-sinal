<h1>Contato</h1>

<div class="container">

	<div class="formulario">

		<form action="contato/enviar" id="form-contato" method="post">

			<label>Nome*<br>
				<input type="text" name="nome" id="contato-nome">
			</label>

			<br>

			<label>Email*<br>
				<input type="email" name="email" id="contato-email">
			</label>

			<br>

			<label class="metade margem">CPF*<br>
				<input type="text" name="cpf" id="contato-cpf">
			</label>

			<label class="metade">Telefone<br>
				<input type="text" name="telefone" id="contato-tel">
			</label>

			<br>

			<label class="alto">Mensagem<br>
				<textarea name="mensagem"></textarea>
			</label>

			<input type="submit" value="enviar &raquo;">

		</form>

	</div>

	<div class="informacoes">

		<h2>
			Central de<br>
			Atendimento
		</h2>

		<h1>11 5525&bull;5700</h1>

		<p>
			Sem Sinal<br>
			Av. Nome da Rua  1000<br>
			Nome do Bairro 0000-000 · São Paulo, SP
		</p>

	</div>

</div>

<?php if ($this->session->flashdata('envio')): ?>
	<script defer>
		$('document').ready( function(){
			alerta("<h2>Mensagem enviada com sucesso!</h2><p>Retornaremos seu contato assim que possível.</p>");
		});
	</script>
<?php endif ?>

<?php if ($this->session->flashdata('erro_validacao')): ?>
	<script defer>
		$('document').ready( function(){
			alerta("<?=$this->session->flashdata('erro_validacao')?>");
		});
	</script>
<?php endif; ?>
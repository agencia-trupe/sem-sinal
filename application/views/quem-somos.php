<h1>
	A SemSinal acredita que estimular<br>
	a proximidade entre o cliente e a operadora <br>
	é capaz de criar relacionamentos <br>
	que revolucionam. 
</h1>

<p>
	<span class="vermelho">Missão:</span> Estimular o mercado criando mecanismos de sinergia nas relações de consumo de forma respeitosa,<br>
	inteligente e divertida proporcionando o encantamento contínuo da sociedade. 
</p>

<p>
	<span class="vermelho">Visão:</span> Valorizar a sociedade como estímulo a melhoria contínua.
</p>

<p>
	<span class="vermelho">Valores:</span> Simplicidade, Respeito e honestidade.
</p>
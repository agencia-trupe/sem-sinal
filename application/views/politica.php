<h1>
	Política de Privacidade
</h1>

<p>
	A SemSinal irá utilizar as infomações pessoais recebidas de clientes e/ou visitantes do site apenas para os fins estabelecidos. 
</p>

<p>
	Consideramos como informações pessoais todas aquelas fornecidas pelo visitante através do site, tais como: nome, endereço,<br>
	 e-mail, telefone e os demais constantes no cadastro.
</p>

<p>
	As informações acima poderão ser utilizadas, somente, para ações que fomentam a melhoria no relacionamento cliente e operadora e, <br>
	a competição saudável entre elas. A SemSinal poderá fazer uso das informações para melhorar a comunicação dos usuários com as operadoras.
</p>

<p>
	Essas informações somente serão obtidas por livre e espontâneo provimento do usuário, que ao fornecê-las consente com seu fornecimento <br>
	às operadoras citadas. 
</p>

<p>
	A SemSinal requisita o número da linha telefônica móvel no cadastro e poderá requisitar caso<br>
	não seja informado para fins de consulta dos serviços oferecidos pelas operadoras. 
</p>

<p>
	A SemSinal não entra em contato com os clientes solicitando senhas de atendimento particular. 
</p>

<p>
	Todos os dados que os usuários visitantes fornecerem no site SemSinal, através de protocolos de segurança, estarão protegidos, afim de assegurar <br>
	sua privacidade e autenticidade à medida que trafegam pela Internet. Entretanto, o site SemSinal não assegura a plena segurança das informações e/ou dados<br>
	enviados pelos usuários e/ou visitantes caso ocorram eventos que ocasionem casos fortuitos ou motivos de força maior, razão pela qual os usuários e/ou visitantes <br>
	expressamente renunciam a qualquer reivindicação contra o site SemSinal em decorrência de perda de dados eventualmente sofridos em virtude de acessos não autorizados <br>
	e/ou quebra de segurança dos sistemas mantidos pelo site SemSinal. Não nos responsabilizamos pelo envio de mesagens de texto para aparelhos celulares originados <br>
	do site SemSinal em vista de tratar-se de canal livre para contato com os clientes. 
</p>

<p>
	A SemSinal toma precaução para evitar o envio indiscriminado de mensagens de texto aos clientes. 
</p>

<p>
	A SemSinal não se responsabiliza pelo envio de e-mails com promessas mentirosas,  ofertas falsas e formulários fraudulentos. <br>
	Em caso de dúvidas relativas à Política de Privacidade contate a SemSinal através do email indicado na área de “Contato” deste site. 
</p>
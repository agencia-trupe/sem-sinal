<div class="coluna">

	<h1>A sua Operadora<br>não te dá mais atenção?</h1>

	<div class="texto">
		<h1 class="sombreado">A CONCORRÊNCIA <span class="maior">DÁ!</span></h1>
		<p>
			Na SemSinal é assim:<br>
			você registra sua insatisfação e,<br>
			ou a sua Operadora atual descobre que você existe,<br>
			ou perde sua conta para a concorrente!
			<br><br>
			<strong>Faça parte! ACESSE!</strong>
		</p>
	</div>

</div>

<div class="coluna box-selecao">

	<h2>Qual é a sua Operadora atual?</h2>

	<a href="cadastro/index/claro" title="Claro" id="link-cel-claro">
		<img src="_imgs/layout/home-celular-claro.png" alt="Claro">
	</a>

	<a href="cadastro/index/tim" title="Tim" id="link-cel-tim">
		<img src="_imgs/layout/home-celular-tim.png" alt="Tim">
	</a>

	<a href="cadastro/index/vivo" title="Vivo" id="link-cel-vivo">
		<img src="_imgs/layout/home-celular-vivo.png" alt="Vivo">
	</a>

	<a href="cadastro/index/oi" title="Oi" id="link-cel-oi">
		<img src="_imgs/layout/home-celular-oi.png" alt="Oi">
	</a>

</div>
<h1>Notícias</h1>

<?php if ($noticias): ?>

	<?php foreach ($noticias as $key => $value): ?>

		<a href="<?=$value->destino?>" title="<?=$value->titulo?>" target="_blank" class="link-noticias<?if($key%2 == 0)echo " margem"?>">
			<h2><?=$value->titulo?></h2>
			<p>
				<?=$value->olho?>
			</p>
			<div class="saiba-mais">saiba mais &raquo;</div>
		</a>
		
	<?php endforeach ?>
	
<?php endif ?>
<h1>Como Funciona?</h1>

<p class="no-top-margin">
	Usar a SemSinal é simples.
</p>

<p>
	Você acessa o portal e indica a sua operadora atual.
</p>

<p>
	Informa seus dados e os dados de sua conta. 
</p>

<p>
	Feito isso, você manifesta sua insatisfação com os serviços prestados pela<br>
	sua operadora e expressa o mínimo que a concorrente deveria fazer para tê-lo como cliente. <br>
	Aproveita e compartilha com seus amigos e familiares ajudando a reivindicar a melhoria nos serviços prestados.
</p>

<h1>
	O RESTO É COM A GENTE!
</h1>

<p class="no-top-margin">
	A SemSinal irá dedicar seus esforços para estabelecer uma comunicação maior entre o fornecedor, concorrente e o consumidor que, <br>
	motivado por um interesse próprio busca a melhoria no serviços prestados a ele, pela transmissibilidade externa direcionada <br>
	às principais operadoras do mercado a saber, Claro S/A, Tim Brasil S/A, Vivo Participações S/A e a Brasil Telecom S/A (OI).
</p>
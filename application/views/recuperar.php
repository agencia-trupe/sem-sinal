<h1>Recuperação de Senha</h1>

<div id="recuperacao">

	<p>
		Informe seu CPF e enviaremos uma senha nova<br>
		para o e-mail cadastrado.	
	</p>

	
	<form id="form-recuperacao" method="post" action="login/recuperar">
		<label>
			CPF: <br>
			<input type="text" name="cpf" id="recup-cpf" autofocus>
		</label>
		<input type="submit" value="enviar &raquo;">
	</form>
	
</div>

<?php if ($this->session->flashdata('erro_validacao')): ?>
	<script defer>
		$('document').ready( function(){
			alerta("<?=$this->session->flashdata('erro_validacao')?>");
		});
	</script>
<?php endif; ?>

<?php if ($this->session->flashdata('validacao_ok')): ?>
	<script defer>
		$('document').ready( function(){
			var retorno = "<h2>Uma nova senha foi enviada para o seu email!</h2>";
			retorno += "<p>Utilize a nova senha para fazer login e lembre-se de alterá-la.</p>";	        
			alerta(retorno);
		});
	</script>
<?php endif; ?>

<script defer>
$('document').ready( function(){
	$('#form-recuperacao').submit( function(){
		if(!$('#recup-cpf').val()){
			alert("Informe seu CPF!");
			return false;
		}
	});
});
</script>
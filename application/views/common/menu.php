
<header>

	<div class="centro">

		<div id="social">
			<a href="" title="Nossa página no Facebook"><img src="_imgs/layout/icone-facebook.png" alt="Facebook"></a>			
			<a href="" title="Nosso perfil no Twitter"><img src="_imgs/layout/icone-twitter.png" alt="Twitter"></a>			
			<a href="" title="Nosso perfil no Google Plus"><img src="_imgs/layout/icone-plus.png" alt="Google Plus"></a>			
			<a href="" title="Nossa página no LinkedIn"><img src="_imgs/layout/icone-linkedin.png" alt="LinkedIn"></a>			
		</div>

		<div id="login">

			<div class="botao camada visivel">
				<?php if ($this->session->userdata('logged_in_cadastro')): ?>
					<?=$this->session->userdata('nome')?>&nbsp<a href="login/logout" class="logout" title="Fazer Logout">sair</a>
				<?php else: ?>
					<a href="#" class="login" title="Fazer Login">LOGIN</a>
				<?php endif ?>
			</div>

			<div class="formulario camada">
				<form method="post" action="login" id="form-login">
					<input type="text" name="login" id="input-login-login" placeholder="CPF">
					<input type="password" name="senha" id="input-login-senha" placeholder="SENHA">
					<input type="submit" value="ok">
				</form>
				<a class="recup" href="login/recuperacao" title="Recuperação de Senha">esqueci minha senha &raquo;</a>
			</div>

			<div class="resposta-login camada <?if($this->session->flashdata('errlogin')) echo' visivel';?>">Erro ao logar. Verifique usuário e senha.</div>

		</div>

		<nav>
			<ul>
				<li><a href="home" title="Página Inicial" id="mn-home" <?if($this->router->class=='home')echo" class='ativo'"?>>home</a></li>
				<li class="gap"><a href="como-funciona" title="Como Funciona" id="mn-como_funciona" <?if($this->router->class=='como_funciona')echo" class='ativo'"?>>como funciona</a></li>
				<li><a href="noticias" title="Notícias" id="mn-noticias" <?if($this->router->class=='noticias')echo" class='ativo'"?>>notícias</a></li>
				<li class="ultimo"><a href="contato" title="Contato" id="mn-contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>contato</a></li>
			</ul>
		</nav>

	</div>
	
</header>

<div class="main centro main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">
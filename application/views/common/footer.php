
    </div> <!-- fim da div main -->

  <footer>

    <div class="centro">

      <div class="coluna">
        <h4>SemSinal</h4>
        <ul>
          <li>&bull;&nbsp;<a href="home" title="Página Inicial">home</a></li>
          <li>&bull;&nbsp;<a href="quem-somos" title="Quem Somos">quem somos</a></li>
          <li>&bull;&nbsp;<a href="como-funciona" title="Como Funciona">como funciona</a></li>
          <li>&bull;&nbsp;<a href="noticias" title="Notíicas">notícias</a></li>
        </ul>
      </div>

      <div class="coluna">
        <h4>Atendimento</h4>
        <ul>
          <li>&bull;&nbsp;<a href="contato" title="Contato">contato</a></li>
          <li>&bull;&nbsp;<a href="faq" title="Perguntas Frequentes">perguntas frequentes</a></li>          
        </ul>
      </div>

      <div class="coluna">
        <h4>Acesso restrito</h4>
        <ul>
          <li>&bull;&nbsp;<a href="minha-conta/<?=$this->session->userdata('operadora_atual')?>" title="Minha Conta">minha conta</a></li>
          <li>&bull;&nbsp;<a href="politica" title="Política de Privacidade">política de privacidade</a></li>          
        </ul>
      </div>

      <div id="assinatura">
        &copy; <?=Date('Y')?> SemSinal - Todos os direitos reservados<br>
        <a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">Criação de Sites : Trupe Agência Criativa</a>
      </div>

    </div>
  
  </footer>

  <div id="alerta"></div>
  
  
  <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
    <script>
      window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  <?endif;?>

  <?JS(array('jquery.maskMoney', 'jquery.maskedinput-1.3.min', 'funcoes', 'front'))?>
  
<!--[if gte IE 9]>
  <style type="text/css">
    #fancybox-outer {
       filter: none;
    }
  </style>
<![endif]-->
  
</body>
</html>
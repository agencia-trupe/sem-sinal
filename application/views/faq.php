<h1>
	Perguntas Frequentes
</h1>

<?php if ($questoes): ?>

	<div id="indice">
		<ul>
			<?php foreach ($questoes as $key => $value): ?>
				<li><?=($key + 1)?>.&nbsp;<a href="faq/#<?=$value->id?>" title="<?=$value->titulo?>"><?=$value->titulo?></a></li>
			<?php endforeach ?>
		</ul>
	</div>

	<div id="conteudo">
		<?php foreach ($questoes as $key => $value): ?>
			<a name="<?=$value->id?>"></a>
			<h2 id="tit_<?=$value->id?>"><?=($key + 1)?>.&nbsp;<?=$value->titulo?></h2>
			<div class="texto">
				<?=$value->texto?>
			</div>
		<?php endforeach ?>
	</div>
	
<?php endif ?>

<script defer>
	$('document').ready( function(){

		$('#indice a').click( function(e){
			e.preventDefault();
			
			var target = $(this).attr('href').split('#').pop();
			
			$('html, body').animate({
				scrollTop : $('#tit_'+target).offset().top
			}, 200);
		});

	});
</script>
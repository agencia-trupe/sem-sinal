<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar <?=$titulo?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir <?=$unidade?></a>
</div>


<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Questão</th>
				<th>Texto</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr id="row_<?=$value->id?>">
				<td><?=$value->titulo?></td>
				<td><?=word_limiter($value->texto, 15)?></td>
				<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
				<td><a class="move" href="#">Mover</a></td>
				<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhuma Questão</h2>

<?endif;?>

<style type="text/css">
	table tbody tr{
		width:100%;
	}
</style>

<script defer>

	$('document').ready( function(){

	    $("table tbody").sortable({
	        update : function () {
	            serial = [];
	            $('table tbody').children('tr').each(function(idx, elm) {
	                serial.push(elm.id.split('_')[1])
	            });
	            $.post(BASE+'/ajax/gravaOrdem', { data : serial , tabela : 'faq'}, function(retorno){
	            	//console.log(retorno);
	            });
	        },
	        helper: function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			},
			handle : $('.move')
	    }).disableSelection();

	});
</script>
<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar <?=$titulo?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir <?=$unidade?></a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título da Questão<br>
			<input type="text" name="titulo" value="<?=$registro->titulo?>">
		</label>

		<label>Resposta<br>
			<textarea class="pequeno basico" name="texto" style="height:150px; resize:none;"><?=$registro->texto?></textarea>
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título da Questão<br>
			<input type="text" name="titulo">
		</label>

		<label>Resposta<br>
			<textarea class="pequeno basico" name="texto" style="height:150px; resize:none;"></textarea>
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>
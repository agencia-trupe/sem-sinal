<h1><?=$titulo?></h1>

<div id="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista <?if($filtro_atual=='todos' && $filtro_preferencia=='todos')echo 'active'?>">Listar Todos os Cadastros</a>
</div>

<div class="filtros">
	Filtrar por operadora atual :
	<a href="painel/cadastros/index/claro/<?=$filtro_preferencia?>" <?if($filtro_atual=='claro')echo "class='ativo'"?> title="Claro">Claro</a>&nbsp;|&nbsp;
	<a href="painel/cadastros/index/oi/<?=$filtro_preferencia?>" <?if($filtro_atual=='oi')echo "class='ativo'"?> title="Oi">Oi</a>&nbsp;|&nbsp;
	<a href="painel/cadastros/index/vivo/<?=$filtro_preferencia?>" <?if($filtro_atual=='vivo')echo "class='ativo'"?> title="Vivo">Vivo</a>&nbsp;|&nbsp;
	<a href="painel/cadastros/index/tim/<?=$filtro_preferencia?>" <?if($filtro_atual=='tim')echo "class='ativo'"?> title="Tim">Tim</a>
</div>

<div class="filtros">
	Filtrar por operadora de preferência :
	<a href="painel/cadastros/index/<?=$filtro_atual?>/claro" <?if($filtro_preferencia=='claro')echo "class='ativo'"?> title="Claro">Claro</a>&nbsp;|&nbsp;
	<a href="painel/cadastros/index/<?=$filtro_atual?>/oi" <?if($filtro_preferencia=='oi')echo "class='ativo'"?> title="Oi">Oi</a>&nbsp;|&nbsp;
	<a href="painel/cadastros/index/<?=$filtro_atual?>/vivo" <?if($filtro_preferencia=='vivo')echo "class='ativo'"?> title="Vivo">Vivo</a>&nbsp;|&nbsp;
	<a href="painel/cadastros/index/<?=$filtro_atual?>/tim" <?if($filtro_preferencia=='tim')echo "class='ativo'"?> title="Tim">Tim</a>
</div>

<br><br>

<a href="<?=base_url('painel/'.$this->router->class.'/extrair/'.$filtro_atual.'/'.$filtro_preferencia)?>" class="add">Download da Lista</a>

<br><br>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Nome</th>
				<th>Operadora Atual</th>
				<th>Operadora de Preferência</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr id="row_<?=$value->id?>">
				<td><?=$value->nome?></td>
				<td><?=ucfirst($value->operadora_atual)?></td>
				<td><?=ucfirst($value->operadora_preferencia)?></td>
				<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
			</tr>

		<? endforeach; ?>

	</table>

<?else:?>

	<h2 style="text-align:center;">Nenhum Registro</h2>

<?endif;?>
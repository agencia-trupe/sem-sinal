<?php

function formataData($data, $tipo){
    if($data != ''){
        if($tipo == 'br2mysql'){
            list($dia,$mes,$ano) = explode('/', $data);
            return $ano.'-'.$mes.'-'.$dia;
        }elseif($tipo == 'mysql2br'){
            list($ano,$mes,$dia) = explode('-', $data);
            return $dia.'/'.$mes.'/'.$ano;
        }
    }else{
        return false;
    }
}

function dia($str){
    if(strpos('/', $str) !== FALSE){
        $xpd = explode('/', $str);
        return $xpd[0];
    }else{
        $xpd = explode('-', $str);
        return $xpd[2];
    }
}

function mes($str, $extenso = false){
    if($extenso){
        $arr = array(
            '01' => 'janeiro',
            '02' => 'fevereiro',
            '03' => 'março',
            '04' => 'abril',
            '05' => 'maio',
            '06' => 'junho',
            '07' => 'julho',
            '08' => 'agosto',
            '09' => 'setembro',
            '10' => 'outubro',
            '11' => 'novembro',
            '12' => 'dezembro',
        );
    }else{
        $arr = array(
            '01' => 'jan',
            '02' => 'fev',
            '03' => 'mar',
            '04' => 'abr',
            '05' => 'mai',
            '06' => 'jun',
            '07' => 'jul',
            '08' => 'ago',
            '09' => 'set',
            '10' => 'out',
            '11' => 'nov',
            '12' => 'dez',
        );        
    }
    if(strpos('/', $str) !== FALSE){
        $xpd = explode('/', $str);
        return $arr[$xpd[1]];
    }elseif(strpos('-', $str) !== FALSE){
        $xpd = explode('-', $str);
        return $arr[$xpd[1]];
    }else{
        return $arr[$str];
    }
}

function ano($str){
    if(strpos('/', $str) !== FALSE){
        $xpd = explode('/', $str);
        return $xpd[2];
    }else{
        $xpd = explode('-', $str);
        return $xpd[0];
    }
}

function olho($text, $maxLength = 200){
    $wordArray = explode(' ', $text);
    if( sizeof($wordArray) > $maxLength ){
        $wordArray = array_slice($wordArray, 0, $maxLength);
        return implode(' ', $wordArray) . '&hellip;';
    }
    return $text;	
}

function relativizaUrl($str){
    $str = str_replace('../', '', $str);
    return $str;
}

function embed($url, $width = '670', $height = '408', $retorna_id = FALSE){

    if (strpos($url, 'youtube.com') !== FALSE) {
        $fonte = 'youtube';
    } elseif(strpos($url, 'youtu.be') !== FALSE) {
        $fonte = 'youtu.be';
    }elseif(strpos($url, 'vimeo.com') !== FALSE){
        $fonte = 'vimeo';
    }else{
        $fonte = 'id_video';
    }

    if($fonte == 'youtube'){
        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
        $video_id = $my_array_of_vars['v'];
        if($video_id){
            if($retorna_id)
                return $video_id;
            $embed_str = "<iframe width='$width' height='$height' src='http://www.youtube.com/embed/".$video_id."' frameborder='0' allowfullscreen></iframe>";
            return $embed_str;
        }else{
            return "Erro ao incorporar o vídeo do Youtube";
        }
    }elseif($fonte == 'vimeo'){
        preg_match('@vimeo.com/([0-9]*)$@i', $url, $found);
        $video_id = $found[1];
        if($video_id){
            if($retorna_id)
                return $video_id;
            $embed_str = "<iframe src='http://player.vimeo.com/video/".$video_id."' width='$width' height='$height' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>";
            return $embed_str;
        }else{
            return "Erro ao incorporar o vídeo do Vimeo";
        }
    }elseif($fonte == 'youtu.be'){
        preg_match('@youtu.be/(.*)$@i', $url, $found);
        $video_id = $found[1];
        if($video_id){
            if($retorna_id)
                return $video_id;
            $embed_str = "<iframe width='$width' height='$height' src='http://www.youtube.com/embed/".$video_id."' frameborder='0' allowfullscreen></iframe>";
            return $embed_str;
        }else{
            return "Erro ao incorporar o vídeo do Youtube";
        }
    }elseif($fonte == 'id_video'){
        $embed_str = "<iframe width='$width' height='$height' src='http://www.youtube.com/embed/".$url."' frameborder='0' allowfullscreen></iframe>";
        return $embed_str;
    }else{
        return "Erro ao incorporar o Vídeo";
    }
}

function videoThumb($url){
    $notfound = "_imgs/layout/no_video.jpg";

    if (strpos($url, 'youtube.com') !== FALSE) {
        $fonte = 'youtube';
    } elseif(strpos($url, 'youtu.be') !== FALSE) {
        $fonte = 'youtu.be';
    }elseif(strpos($url, 'vimeo.com') !== FALSE){
        $fonte = 'vimeo';
    }else{
        $fonte = 'id_video';
    }    

    if($fonte == 'youtube'){
        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
        $video_id = $my_array_of_vars['v'];
        if($video_id){
            $thumb = "http://img.youtube.com/vi/".$video_id."/2.jpg";
            return $thumb;
        }else{
            return $notfound;
        }
    }elseif($fonte == 'vimeo'){
        preg_match('@vimeo.com/([0-9]*)$@i', $url, $found);
        $video_id = $found[1];
        if($video_id){
            $xml = simplexml_load_file("http://vimeo.com/api/v2/video/".$video_id.".xml");
            foreach($xml->video as $node){
                $thumb = $node->thumbnail_small;
            }
            return $thumb;
        }else{
            return $notfound;
        }
    }elseif($fonte == 'youtu.be'){
        preg_match('@youtu.be/(.*)$@i', $url, $found);
        $video_id = $found[1];
        if($video_id){
            $thumb = "http://img.youtube.com/vi/".$video_id."/2.jpg";
            return $thumb;
        }else{
            return $notfound;
        }
    }elseif($fonte == 'id_video'){
        if($url){
            $thumb = "http://img.youtube.com/vi/".$url."/2.jpg";
            return $thumb;
        }else{
            return $notfound;
        }        
    }else{
        return $notfound;
    }
}

function limpaGMaps($str, $removeLink = TRUE){
    if($removeLink)
        $str = preg_replace('~<br /><small>(.*)</small>~', '', $str);

    return mysql_real_escape_string(stripslashes($str));
}

function viewGMaps($str, $width = FALSE, $height = FALSE){

    $str = stripslashes(htmlspecialchars_decode($str));

    if($width)
        $str = preg_replace('~width="(\d+)"~', 'width="'.$width.'"', $str);
    if($height)
        $str = preg_replace('~height="(\d+)"~', 'height="'.$height.'"', $str);

    return $str;
}

function cpfValido($cpf){
    //Etapa 1: Cria um array com apenas os digitos numéricos, isso permite receber o cpf em 
    //diferentes formatos como "000.000.000-00", "00000000000", "000 000 000 00" etc...
    $j=0;
    for($i=0; $i<(strlen($cpf)); $i++)
        {
            if(is_numeric($cpf[$i]))
                {
                    $num[$j]=$cpf[$i];
                    $j++;
                }
        }
    //Etapa 2: Conta os dígitos, um cpf válido possui 11 dígitos numéricos.
    if(count($num)!=11)
        {
            $isCpfValid=false;
        }
    // Etapa 3: Combinações como 00000000000 e 22222222222 embora não sejam cpfs reais resultariam 
    // em cpfs válidos após o calculo dos dígitos verificares e por isso precisam ser filtradas nesta parte.
    else
        {
            for($i=0; $i<10; $i++)
                {
                    if ($num[0]==$i && $num[1]==$i && $num[2]==$i && $num[3]==$i && $num[4]==$i && $num[5]==$i && $num[6]==$i && $num[7]==$i && $num[8]==$i)
                        {
                            $isCpfValid=false;
                            break;
                        }
                }
        }
    //Etapa 4: Calcula e compara o primeiro dígito verificador.
    if(!isset($isCpfValid))
        {
            $j=10;
            for($i=0; $i<9; $i++)
                {
                    $multiplica[$i]=$num[$i]*$j;
                    $j--;
                }
            $soma = array_sum($multiplica); 
            $resto = $soma%11;          
            if($resto<2)
                {
                    $dg=0;
                }
            else
                {
                    $dg=11-$resto;
                }
            if($dg!=$num[9])
                {
                    $isCpfValid=false;
                }
        }
    //Etapa 5: Calcula e compara o segundo dígito verificador.
    if(!isset($isCpfValid))
        {
            $j=11;
            for($i=0; $i<10; $i++)
                {
                    $multiplica[$i]=$num[$i]*$j;
                    $j--;
                }
            $soma = array_sum($multiplica);
            $resto = $soma%11;
            if($resto<2)
                {
                    $dg=0;
                }
            else
                {
                    $dg=11-$resto;
                }
            if($dg!=$num[10])
                {
                    $isCpfValid=false;
                }
            else
                {
                    $isCpfValid=true;
                }
        }
    return $isCpfValid;                 
}

function dataValida($data){
    return preg_match('/^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$/', $data);
}

function emailValido($email){
    return preg_match('/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/', $email);
}

function criptografar($senha){
    $CI =& get_instance();
    return sha1($senha.$CI->config->item('encryption_key'));
}

function limpaValor($valor){
    $valor = str_replace('R$ ', '', $valor);
    $valor = str_replace('.', '', $valor);
    $valor = str_replace(',', '.', $valor);
    return $valor;
}

function float2money($float){
    $float = str_replace(',', '', $float);
    $float = str_replace('.', ',', $float);
    return 'R$ '.trim($float);
}

function money2float($money){
    $money = str_replace('R$', '', $money);
    $money = trim($money);
    $money = str_replace('.', '', $money);
    $money = str_replace(',', '.', $money);
    return (float) $money;
}

function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, strlen($alphabet)-1); //use strlen instead of count
        $pass[$i] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}
?>

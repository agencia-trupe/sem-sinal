var alerta = function(mensagem){

    $.fancybox(
        "<h2>"+mensagem+"</h2>",
        {
            'autoDimensions'    : false,
            'width'             : 362,
            'height'            : 'auto',
            'transitionIn'      : 'none',
            'transitionOut'     : 'none',
            'overlayColor'		: 'rgba(119, 119, 119, 0.6)',
            'padding'			: 0,
            onComplete          : function(){
            	$('html, body').animate({
            		scrollTop : ($('#fancybox-wrap').offset().top - 100)
            	}, 200);
            }
        }
    );	

}
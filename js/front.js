$('document').ready( function(){

	$('#input-login-login').mask("999.999.999-99");
	$('#recup-cpf').mask("999.999.999-99");

	$('#form .c-align label').click( function(){
		$('#form .c-align label.selecionado').removeClass('selecionado');
		$(this).toggleClass('selecionado');
	});

	$('#form .main-form label.portar input[type=checkbox]').click( function(){
		if($(this).is(':checked')){
			$(this).parent('label').addClass('selecionado');
		}else{
			$(this).parent('label').removeClass('selecionado');
		}
	});

	$('#form .main-form .coluna label input[type=checkbox]').click( function(){
		if($(this).is(':checked')){
			$(this).parent('label').addClass('selecionado');
		}else{
			$(this).parent('label').removeClass('selecionado');
		}
	});

	$('#form .caixa-celulares label input[type=radio]').click( function(){
		var ctx = $(this).parent().parent('.caixa-celulares');
		if($(this).is(':checked')){
			$('label.selecionado', ctx).removeClass('selecionado');
			$(this).parent('label').addClass('selecionado').removeClass('deselecionado');
			$('label', ctx).not('.selecionado').addClass('deselecionado');
		}else{
			$(this).parent('label').removeClass('selecionado');
		}

	});

	$('#form form #input-ddd').mask("99");

	$('#form form #input-fone').mask("9999-9999?9");

	$('#form form #input-cpf').mask("999.999.999-99");

	$('#form form #input-fixo').mask("(99) 9999-9999");

	$('#form form #input-cep').mask("99999-999");

	$('#form form #input-data').mask("99/99/9999");


	$('#form form #media-gasto').maskMoney({
		symbol: 'R$',
		showSymbol: true,
		symbolStay: true,
		thousands: '.',
		decimal: ',',
		allowNegative: false
	});

	$('#sel-uf').change( function(){

		var estado_id = $(this).val();
		var options = "<option value=''>Selecione a Cidade</option>";

		$.post(BASE+'ajax/pegaCidades', { estado : estado_id }, function(retorno){

			retorno = JSON.parse(retorno);

			retorno.map( function(self){
				options += "<option value='"+self.id+"'>"+self.nome+"</option>";
			});

			$('#sel-cidade').html(options);

		});
	});

	$('.main-cadastro #form #form-cadastro').submit( function(e){

		var mensagem = "";

		if(!$("input[name='operadora_preferencia']:checked").val() && !mensagem)
       		var mensagem = 'Informe a Operadora de sua Preferência!';

		if(!$("#input-ddd").val() && !mensagem)
       		var mensagem = 'Informe o DDD de seu telefone atual!';

		if(!$("#input-fone").val() && !mensagem)
       		var mensagem = 'Informe o seu número de telefone atual!';

       	if(!$("#media-gasto").val() && !mensagem)
       		var mensagem = 'Informe seu o gasto médio mensal com seu celular!';

       	if(!$("input[name='tipo_plano']:checked").val() && !mensagem)
       		var mensagem = 'Informe qual é o tipo do seu plano atual!';

       	if((!$("input[name='nome']").val() || $("input[name='nome']").val().length < 3)&& !mensagem)
       		var mensagem = 'Informe seu Nome!';

       	if(!$("#input-cpf").val() && !mensagem)
       		var mensagem = 'Informe seu CPF!';

       	if(!$("#input-fixo").val() && !mensagem)
       		var mensagem = 'Informe seu Telefone Fixo!';

       	if(!$("input[name=endereco]").val() && !mensagem)
       		var mensagem = 'Informe seu Endereço!';

       	if(!$("input[name=numero]").val() && !mensagem)
       		var mensagem = 'Informe o Número de seu Endereço!';

       	if(!$("#sel-uf").val() && !mensagem)
       		var mensagem = 'Informe seu estado!';

       	if(!$("#sel-cidade").val() && !mensagem)
       		var mensagem = 'Informe sua cidade!';

       	if(!$("#input-cep").val() && !mensagem)
       		var mensagem = 'Informe seu CEP!';

       	if(!$("#input-data").val() && !mensagem)
       		var mensagem = 'Informe sua data de nascimento!';

       	if(!$("#input-email").val() && !mensagem)
       		var mensagem = 'Informe seu email!';

       	if($('#input-ed-form').length == 0){

	       	if(!$("#input-senha").val() && !mensagem)
	       		var mensagem = 'Informe sua senha de acesso!';

	       	if(!$("#input-confirmacao_senha").val() && !mensagem)
	       		var mensagem = 'Informe novamente sua senha de acesso!';

	       	if(($("#input-confirmacao_senha").val() != $("#input-senha").val()) && !mensagem)
	       		var mensagem = 'A confirmação de senha não confere com a senha!';

		}

    	if(mensagem != ""){
    		e.preventDefault();
    		alerta(mensagem);
    	}

	});

	if($('header #login .resposta-login.visivel').length){
		setTimeout( function(){
			$('header #login .botao.visivel').removeClass('visivel');
			setTimeout( function(){
				$('header #login .resposta-login.visivel').removeClass('visivel');
				$('header #login .formulario').addClass('visivel');
			}, 1000);
		}, 50);
	}

	$('header #login .botao a.login').live('click', function(e){
		e.preventDefault();

		$('#login .visivel').removeClass('visivel');
		$('#login .formulario').addClass('visivel');
	});

	$('#form-login').submit( function(){

		if(!$('#input-login-login').val()){
			alerta("Informe seu login!");
			return false;
		}

		if(!$('#input-login-senha').val()){
			alerta("Informe sua senha!");
			return false;
		}

	});

	$('#contato-cpf').mask("999.999.999-99");
	$('#contato-tel').mask("(99) 9999-9999");

	$('#form-contato').submit( function(){

		if(!$('#contato-nome').val()){
			alerta("Informe seu Nome!");
			return false;
		}

		if(!$('#contato-email').val()){
			alerta("Informe seu Email!");
			return false;
		}

		if(!$('#contato-cpf').val()){
			alerta("Informe seu CPF!");
			return false;
		}


	});

});